# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120714223415) do

  create_table "band_genre_relationships", :force => true do |t|
    t.integer  "band_id"
    t.integer  "genre_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "band_genre_relationships", ["band_id", "genre_id"], :name => "index_band_genre_relationships_on_band_id_and_genre_id", :unique => true

  create_table "bands", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "comments", :force => true do |t|
    t.integer  "user_id"
    t.integer  "event_id"
    t.string   "content"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "comments", ["user_id", "event_id", "created_at"], :name => "index_comments_on_user_id_and_event_id_and_created_at", :unique => true

  create_table "event_band_relationships", :force => true do |t|
    t.integer  "event_id"
    t.integer  "band_id"
    t.integer  "state"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "playlist_id"
    t.integer  "order"
  end

  add_index "event_band_relationships", ["event_id", "band_id"], :name => "index_event_band_relationships_on_event_id_and_band_id", :unique => true

  create_table "events", :force => true do |t|
    t.string   "name"
    t.datetime "datetime"
    t.text     "description"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.integer  "user_id"
    t.float    "lat"
    t.float    "lon"
    t.string   "poster_file_name"
    t.string   "poster_content_type"
    t.integer  "poster_file_size"
    t.datetime "poster_updated_at"
  end

  add_index "events", ["lat", "lon", "datetime"], :name => "index_events_on_lat_and_lon_and_datetime", :unique => true

  create_table "fan_band_relationships", :force => true do |t|
    t.integer  "user_id"
    t.integer  "band_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "fan_band_relationships", ["user_id", "band_id"], :name => "index_fan_band_relationships_on_user_id_and_band_id", :unique => true

  create_table "genres", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "genres", ["name"], :name => "index_genres_on_name", :unique => true

  create_table "user_band_relationships", :force => true do |t|
    t.integer  "user_id"
    t.integer  "band_id"
    t.boolean  "priviledges"
    t.string   "role"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "user_event_relationships", :force => true do |t|
    t.integer  "user_id"
    t.integer  "event_id"
    t.integer  "state"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "user_event_relationships", ["event_id"], :name => "index_user_event_relationships_on_event_id"
  add_index "user_event_relationships", ["user_id", "event_id"], :name => "index_user_event_relationships_on_user_id_and_event_id", :unique => true
  add_index "user_event_relationships", ["user_id"], :name => "index_user_event_relationships_on_user_id"

  create_table "user_genre_relationships", :force => true do |t|
    t.integer  "user_id"
    t.integer  "genre_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "user_genre_relationships", ["user_id", "genre_id"], :name => "index_user_genre_relationships_on_user_id_and_genre_id", :unique => true

  create_table "user_user_relationships", :force => true do |t|
    t.integer  "follower_id"
    t.integer  "followed_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "user_user_relationships", ["followed_id"], :name => "index_user_user_relationships_on_followed_id"
  add_index "user_user_relationships", ["follower_id", "followed_id"], :name => "index_user_user_relationships_on_follower_id_and_followed_id", :unique => true
  add_index "user_user_relationships", ["follower_id"], :name => "index_user_user_relationships_on_follower_id"

  create_table "users", :force => true do |t|
    t.string   "fullname"
    t.string   "email"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.string   "twitter_uid"
    t.string   "city"
    t.date     "birthdate"
    t.string   "password_digest"
    t.string   "remember_token"
    t.string   "facebook_uid"
    t.string   "profilepic_file_name"
    t.string   "avatar_remote_url"
    t.string   "profilepic_content_type"
    t.integer  "profilepic_file_size"
    t.datetime "profilepic_updated_at"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["remember_token"], :name => "index_users_on_remember_token"

end
