class AddIndexToUserIdEventIdInComments < ActiveRecord::Migration
  def change
    add_index :comments, [:user_id, :event_id], unique: true
  end
end
