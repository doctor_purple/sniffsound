class AddIndexToBandGenreRelationships < ActiveRecord::Migration
  def change
    add_index :band_genre_relationships, [:band_id, :genre_id], unique:true
  end
end
