class CreateEventBandRelationships < ActiveRecord::Migration
  def change
    create_table :event_band_relationships do |t|
      t.integer :event_id
      t.integer :band_id
      t.integer  :state
      t.timestamps
    end
    
    add_index :event_band_relationships, [:event_id, :band_id], unique:true
  end
end
