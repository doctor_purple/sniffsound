class RenameDescrizioneAndScalettaToDescriptionAndPlaylistInEvents < ActiveRecord::Migration
  def change
    rename_column :events, :descrizione, :description
    rename_column :events, :scaletta_id, :playlist_id
  end
end
