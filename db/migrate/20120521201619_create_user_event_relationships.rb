class CreateUserEventRelationships < ActiveRecord::Migration
  def change
    create_table :user_event_relationships do |t|
      t.integer :user_id
      t.integer :event_id
      t.integer :state

      t.timestamps
    end
    
    add_index :user_event_relationships, [:user_id]
    add_index :user_event_relationships, [:event_id]
    add_index :user_event_relationships, [:user_id,:event_id], unique: true
  end
end
