class AddUseridPositionToEvents < ActiveRecord::Migration
  def change
    add_column :events, :user_id, :integer
    add_column :events, :lat, :float
    add_column :events, :lon, :float
  end
end
