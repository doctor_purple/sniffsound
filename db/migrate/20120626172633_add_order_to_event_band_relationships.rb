class AddOrderToEventBandRelationships < ActiveRecord::Migration
  def change
    add_column :event_band_relationships, :order, :integer
  end
end
