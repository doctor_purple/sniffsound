class ChangeIndexOnComments < ActiveRecord::Migration
  def up
    remove_index :comments, [:user_id, :event_id]
    add_index :comments, [:user_id, :event_id, :created_at], unique: true
  end

  def down
    remove_index :comments, [:user_id, :event_id, :created_at]
    add_index :comments, [:user_id, :event_id], unique: true
  end
end
