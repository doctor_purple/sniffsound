class AddPlaylistIdToEventBandRelationships < ActiveRecord::Migration
  def change
    add_column :event_band_relationships, :playlist_id, :integer
  end
end
