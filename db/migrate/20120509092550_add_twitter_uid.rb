class AddTwitterUid < ActiveRecord::Migration
  def change
    add_column :users, :twitter_uid, :string
    add_index :users, :twitter_uid, unique: true
    
    add_column :users, :city, :string
    add_column :users, :birthdate, :date
  end
end
