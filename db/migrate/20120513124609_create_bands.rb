class CreateBands < ActiveRecord::Migration
  def change
    create_table :bands do |t|
      t.string :name
      t.text :description
      t.string :type_of_music

      t.timestamps
    end
  end
end
