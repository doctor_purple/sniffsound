class CreateFanBandRelationships < ActiveRecord::Migration
  def change
    create_table :fan_band_relationships do |t|
      t.integer :user_id
      t.integer :band_id

      t.timestamps
    end
  end
end
