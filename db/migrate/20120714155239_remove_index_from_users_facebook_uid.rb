class RemoveIndexFromUsersFacebookUid < ActiveRecord::Migration
  def up
    remove_index :users, [:facebook_uid]
  end

  def down
    add_index :users, [:facebook_uid], unique:true
  end
end
