class RemoveAdministratorFromBands < ActiveRecord::Migration
  def up
    remove_column :bands, :administrator
  end

  def down
    add_column :bands, :administrator, :integer
  end
end
