class AddIndexToUsers < ActiveRecord::Migration
  def change
    
      add_index :user_user_relationships, :follower_id
      add_index :user_user_relationships, :followed_id
      add_index :user_user_relationships, [:follower_id, :followed_id], unique: true
  end
end
