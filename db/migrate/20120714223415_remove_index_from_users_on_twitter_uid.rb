class RemoveIndexFromUsersOnTwitterUid < ActiveRecord::Migration
  def up
    remove_index :users,[:twitter_uid]
  end

  def down
    add_index :users,[:twitter_uid], unique: true
  end
end
