class ChangeAvatarColumnsNames < ActiveRecord::Migration
  def change
    rename_column :users, :avatar_file_name, :profilepic_file_name
    rename_column :users, :avatar_content_type, :profilepic_content_type
    rename_column :users, :avatar_file_size, :profilepic_file_size
    rename_column :users, :avatar_updated_at, :profilepic_updated_at
  end
end
