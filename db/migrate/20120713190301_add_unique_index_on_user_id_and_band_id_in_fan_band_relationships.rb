class AddUniqueIndexOnUserIdAndBandIdInFanBandRelationships < ActiveRecord::Migration
  def change
    add_index :fan_band_relationships, [:user_id, :band_id], unique: true
  end
end
