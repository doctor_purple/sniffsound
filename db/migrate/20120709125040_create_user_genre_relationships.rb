class CreateUserGenreRelationships < ActiveRecord::Migration
  def change
    create_table :user_genre_relationships do |t|
      t.integer :user_id
      t.integer :genre_id

      t.timestamps
    end
  end
end
