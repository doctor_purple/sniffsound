class RemovePlaylistIdFromEvents < ActiveRecord::Migration
  def change
    remove_column :events, :playlist_id
  end
end
