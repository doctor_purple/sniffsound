class RemoveColumnTypeOfMusicFromBands < ActiveRecord::Migration
  def up
    remove_column :bands, :type_of_music
  end

  def down
    add_column :bands, :type_of_music, :string
  end
end
