class AddIndexToUserGenreRelationships < ActiveRecord::Migration
  def change
    add_index :user_genre_relationships, [:user_id, :genre_id], unique:true
  end
end
