class AddIndexToEvents < ActiveRecord::Migration
  def change
    add_index :events, [:lat,:lon,:datetime], unique:true
  end
end
