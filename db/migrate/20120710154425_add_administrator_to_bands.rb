class AddAdministratorToBands < ActiveRecord::Migration
  def change
    add_column :bands, :administrator, :integer
  end
end
