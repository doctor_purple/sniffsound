class CreateBandGenreRelationships < ActiveRecord::Migration
  def change
    create_table :band_genre_relationships do |t|
      t.integer :band_id
      t.integer :genre_id

      t.timestamps
    end
  end
end
