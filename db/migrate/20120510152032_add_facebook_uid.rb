class AddFacebookUid < ActiveRecord::Migration
  def change
    add_column :users,:facebook_uid,:string
    add_index :users, :facebook_uid, unique: true
  end
end
