class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.datetime :datetime
      t.integer :scaletta_id
      t.integer :band_id
      t.text :descrizione

      t.timestamps
    end
  end
end
