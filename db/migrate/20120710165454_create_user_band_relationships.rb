class CreateUserBandRelationships < ActiveRecord::Migration
  def change
    create_table :user_band_relationships do |t|
      t.integer :user_id
      t.integer :band_id
      t.boolean :priviledges
      t.string :role

      t.timestamps
    end
  end
end
