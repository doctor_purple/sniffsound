
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Utenti
User.create!(fullname: "Provatore",
             email: "provatore@sniffsound.org",
             password: "password",
             password_confirmation: "password",
             birthdate: "06/02/1989",
             city: Faker::Address.city)
99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@sniffsound.org"
password  = "password"
User.create!(fullname: name,
             email: email,
             password: password,
             password_confirmation: password,
             birthdate: "06/02/1989",
                 city: Faker::Address.city)
end

#Follower
15.times do |n|
  User.find_by_id(Integer(rand(1..80))).follow!(User.find_by_id(Integer(rand(1..80))))
end

# Eventi degli utenti
users = User.all(limit:6).each do |user|
  10.times do |n|
      description = Faker::Lorem.sentence(5)
      name = Faker::Lorem.sentence(2)
      datetime = DateTime::parse(Integer(rand(1..12)).to_s+'/'+Integer(rand(1..12)).to_s+'/'+Integer(rand(2010..2013)).to_s+' 20:30')
      lat = 0.000001*rand(40000000..50000000)
      lon = 0.000001*rand(1000000..18000000)
      user.events.create(name:name, description:description, datetime:datetime, lat:lat, lon:lon)
  end
end

# Generi
Genre.create([{name:"Rock"}, 
              {name:"Pop"},
              {name:"Jazz"},
              {name:"Hip-Hop"},                           
              {name:"Elettronica"},
              {name:"Classica"},
              {name:"Alternativa"}])

#Band
30.times do |n|
    description = Faker::Lorem.sentence(5)
    name = "#{Band } #{Faker::Lorem.sentence(2)}"
    band = Band.new(description: description, name: name)
    
    user = User.create!(fullname: "Saber-#{n+100}",
             email: "example-#{n+100}-saber@sniffsound.org",
             password: "password",
             password_confirmation: "password",
             birthdate: "06/02/1989",
                 city: Faker::Address.city)
   band.user_band_relationships.build(user_id: user.id, role: "Dj", priviledges: true)
   
   user = User.create!(fullname: "Seneca-#{n+100}",
             email: "example-#{n+100}-seneca@sniffsound.org",
             password: "password",
             password_confirmation: "password",
             birthdate: "06/02/1989",
                 city: Faker::Address.city)
   band.user_band_relationships.build(user_id: user.id, role: "Pianoforte")
   
   user = User.create!(fullname: "Beast-#{n+100}",
             email: "example-#{n+100}-beast@sniffsound.org",
             password: "password",
             password_confirmation: "password",
             birthdate: "06/02/1989",
                 city: Faker::Address.city)
   band.user_band_relationships.build(user_id: user.id, role: "Max-sp5")
   
   user = User.create!(fullname: "Leader-#{n+100}",
             email: "example-#{n+100}-leader@sniffsound.org",
             password: "password",
             password_confirmation: "password",
             birthdate: "06/02/1989",
                 city: Faker::Address.city)
   band.user_band_relationships.build(user_id: user.id, role: "Suona la gente")
   
   band.save
end

#commenti agli eventi
events = Event.all
events.each do |e|
  rand(1..5).times do
    e.comment(Faker::Lorem.sentence(5)[0..30],Integer(rand(1..99)))
  end
  
  #assegno un paio di band agli eventi
  id = Integer(rand(1..49))
  e.event_band_relationships.build(band_id:id)
  e.event_band_relationships.build(band_id:id+1)
  e.save
end

              





