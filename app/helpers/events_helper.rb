module EventsHelper
 def band_selection_lists
   @bands = Band.where("id NOT IN (SELECT band_id FROM event_band_relationships WHERE event_id=?)", @event.id).to_json.html_safe
   @bands_participating = @event.bands.all.to_json.html_safe
 end
end
