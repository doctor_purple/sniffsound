class UserMailer < ActionMailer::Base
  default :from => "sniffsounds@gmail.com"
  
  def registration_confirmation(user)
    @user = user
    mail(:to => user.email, :subject => "Benvenuto in Sniffsound!")
  end
end