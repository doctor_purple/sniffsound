class SessionsController < ApplicationController
  def new 
  end
  
  def create 
   reset_session #cancella tutti i dati della sessione prima che ne venga creata una nuova della sessione prima che ne venga creata una nuova
   user = User.find_by_email(params[:session][:email]) #può essere nil, true o false. Nel caso nil vale come un false
   if user && user.authenticate(params[:session][:password])
     if params[:remember_me] #se è presente la chech "rimani connesso", usa il permanent cookie
       sign_in user
     else #se non è presente, cancello il cookie vecchio, lo ricreo e lo uso solo per quella sessione
       new_remember_token = user.create_remember_token
       user.update_attribute(:remember_token, new_remember_token)
       sign_in_without_permanent_cookies user
     end   
    else
      flash[:error] = 'login errato'
    end 
    
    redirect_to root_path #torna sempre alla home e fa il controllo su signed_in 
  end
  
  def destroy
    sign_out
    redirect_to root_path  
  end   
end
