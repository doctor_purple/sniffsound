class UsersController < ApplicationController
  require 'date'
  before_filter :signed_in_user, only: [:edit, :update, :index]
  before_filter :correct_user, only: [:edit, :update] #l'utente può modificare solo la sua pagine
  
  
  def new
    @title = "Sign up"
    
    auth_hash = request.env["omniauth.auth"]
    if auth_hash.nil?
      redirect_url = '/auth/'+params['p'];
      redirect_to redirect_url
      
    #TWITTER---------------------------------------------------------------------------------------------------------------------------------------  
    elsif params['provider']=='twitter'
      city = city_splitting(auth_hash)
      
      @avatar = auth_hash['info']['image']
      @user = User.new(twitter_uid:auth_hash['uid'], 
                       fullname:auth_hash['info']['name'],
                       city:city,
                       avatar_remote_url:auth_hash['info']['image'])
                 
    #FACEBOOK----------------------------------------------------------------------------------------------------------------------------------------      
    elsif params['provider']=='facebook'
      city = city_splitting(auth_hash)
      birthdate = birthday_parsing(auth_hash)
       
      @avatar = auth_hash['info']['image']
      @user = User.new(facebook_uid:auth_hash['credentials']['token'], 
                       fullname:auth_hash['info']['name'],
                       email:auth_hash['info']['email'],
                       city:city,
                       birthdate:birthdate,
                       avatar_remote_url:auth_hash['info']['image'])
    end
  end
  
  def show
    @user = User.find(params[:id])
    @events = @user.events
    @number_events_created = @events.length #numero eventi creati dall'utente
    @participating_events = Event.search_events_participating(@user) #eventi a cui l'utente parteciperà
    @participated_events = Event.search_events_participated(@user) #eventi a cui l'utente ha partecipato
    @events_most_popular = Event.search_most_popular #restituisce i primi 10 eventi più partecipati in totale
    @events_most_popoluar_followed_users = #
    @comments = @user.comments
    @last_comments = @user.comments.order('created_at DESC').limit(3)
    
    
    @followed_users = @user.followed_users.paginate(page: params[:page])
    @followers = @user.followers.paginate(page: params[:page])

  end
  
  #ottengo tutti gli utenti. mi serve nel search
  def index
      @users = User.paginate(page: params[:page])
  end
  
  def create
    @user = User.new(params[:user])
    if @user.save
      if not params[:genre].nil?
        @user.saveFavouriteGenres(params[:genre])
      end
      sign_in_without_permanent_cookies @user #la prima volta salva il cookie solo per la sessione
      flash[:success] = "Benvenuto in SniffSound!"
      #UserMailer.registration_confirmation(@user).deliver
      redirect_to root_path #torno alla home, che (visto che ora l'utente è loggato) reidirizzerà alla dashboard
    else
      render 'new' #ritorna al new con messaggi d'errore
    end
  end
  
  def failure
    redirect_to root_url, :alert => "Authentication error: #{params[:message].humanize}"
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def update
     @user = User.find(params[:id])
    if @user.update_attributes_and_genres(params)
      #faccio il salvataggio nel DB, con update_attributes che deve di nuovo passare tutti i validatori del modello
      flash[:success] = "Profilo aggiornato!"
      sign_in @user
      redirect_to root_path
    else
      render 'edit'  #ritorna all'edit per le modifiche
    end
  end
  
  
  def search_autocompletion
    @users = User.search(params)
    #@users = User.all
    respond_to do |format|
      format.html
      format.js
    end
  end
  
  
  
  
private  
# -----------------------------------------------METODI PRIVATI-------------------------------------------
  def correct_user #controllo id utente = current_user
    @user = User.find(params[:id])
    if !current_user?(@user) #controllo id utente = current_user
      redirect_to root_path
    end
  end
  
  def city_splitting(auth_hash) #gestione dello split della città 
    #gestione della città e dello split
     city = auth_hash['info']['location'] 
     if city
      city = city.split(', ')[0]
     end    
  end
  
  def birthday_parsing(auth_hash) #gestione della data di nascita
    birthdate = auth_hash['extra']['raw_info']['birthday']
      #render text: birthdate 
      if birthdate
        birthdate = Date.strptime(birthdate, '%m/%d/%Y') #hash-->Date object
      end
  end
  
end
