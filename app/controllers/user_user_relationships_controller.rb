class UserUserRelationshipsController < ApplicationController
  before_filter :signed_in_user
  def create
    @user = User.find_by_id(params[:followed_id])
    logger.info(@user.id)
    @relationship = current_user.follow!(@user)
    respond_to do |format|
      format.html {redirect_to request.referer}
      format.js
    end
  end

  def destroy
    @user = User.find_by_id(current_user.user_user_relationships.find_by_id(params[:id]).followed_id)  
    current_user.unfollow!(params[:id])
    respond_to do |format|
      format.html {redirect_to request.referer}
      format.js
    end
  end
end
  
  
  
  
  
  
  
  
  
  
  
