class FanBandRelationshipsController < ApplicationController
   before_filter :signed_in_user
  def create
    @band = Band.find_by_id(params[:band_id])
    @relationship = current_user.fan!(@band)
    respond_to do |format|
      format.html {redirect_to request.referer}
      format.js
    end
  end

  def destroy
    @band = Band.find_by_id(current_user.fan_band_relationships.find_by_id(params[:id]).band_id)
    current_user.unfan!(params[:id])
    respond_to do |format|
      format.html {redirect_to request.referer}
      format.js
    end
  end
end
