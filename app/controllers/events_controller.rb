class EventsController < ApplicationController
  before_filter :signed_in_user, only: [:new,:create,:edit,:destroy,:update]
  before_filter :correct_user, only: [:edit, :update]
  def new
    
    @now = DateTime.now.strftime('%d/%m/%Y')
    @event = Event.new
    @bands_participating = @event.band_selection_lists_participating
    @bands = @event.band_selection_lists_not_participating
  end

  def create
    
    date = params[:giornoHidden]
    hour = params[:oreHidden]
    minute = params[:minutiHidden]
    
    if !date.blank? || !hour.blank? || !minute.blank?
        datetime = Event.splitting_time(date, hour, minute)
  
   
        @event = current_user.events.build(name: params[:event][:name],
                                           poster: params[:event][:poster],
                                           datetime: datetime,
                                           description: params[:event][:description],
                                           lat: params[:event][:lat],
                                           lon: params[:event][:lon])
        #Costruisco le relazioni evento - band
        @event.build_bands(params)
    end
    if @event.save_unique
      flash[:success] = "Evento creato correttamente!"
      redirect_to event_path(@event)
    else
        @bands_participating = @event.band_selection_lists_participating
        @bands = @event.band_selection_lists_not_participating
        @now = DateTime.now.strftime('%d/%m/%Y')
      render 'new'
    end
    
  end

  def edit
    @event = Event.find(params[:id])
    #devo ritrasformare la date in un formato compatibile con il date e il time picker
    datetime = @event.datetime
    @date = datetime.strftime('%d/%m/%Y')
    @hour = datetime.strftime('%l')
    @minute = datetime.strftime('%M')
    @meridian = datetime.strftime('%p')
    
    #-----------------------band------------------------------------
    @bands_participating = @event.band_selection_lists_participating
    @bands = @event.band_selection_lists_not_participating
  end

  def update
    @event = Event.find(params[:id])
    @event.update_bands(params)
    
    date = params[:giornoHidden]
    hour = params[:oreHidden]
    minute = params[:minutiHidden]
    
    
    if !date.blank? && !hour.blank? && !minute.blank?
        datetime = Event.splitting_time(date, hour, minute)
   else
     datetime = DateTime.parse("#{date.to_s} #{hour}:#{minute}")
   end
    
    if @event.update_attributes(name: params[:event][:name],
                                poster: params[:event][:poster],
                                datetime: datetime,
                                description: params[:event][:description],
                                lat: params[:event][:lat],
                                lon: params[:event][:lon])
      flash[:success] = "Evento aggiornato!"
      redirect_to event_path(@event)
    else
    #devo ritrasformare la date in un formato compatibile con il date e il time picker
    datetime = @event.datetime
    @date = datetime.strftime('%d/%m/%Y')
    @hour = datetime.strftime('%l')
    @minute = datetime.strftime('%M')
    @meridian = datetime.strftime('%p')
    
    #-----------------------band------------------------------------
    @bands_participating = @event.band_selection_lists_participating
    @bands = @event.band_selection_lists_not_participating
      render 'edit'
    end
    
  end

  def destroy
    @event = Event.find(params[:id])
    url = request.referer
    if("#{request.referer}/edit".include?(edit_event_path(@event)))
      url = root_path
    end
    
    @event.destroy
    
    redirect_to url
  end

  def show
    @event = Event.find(params[:id])
    
    #splitting per la visualizzazione
    datetime = @event.datetime
    @date = datetime.strftime('%d/%m/%Y')
    @hour = datetime.strftime('%H')
    @minute = datetime.strftime('%M')
    
    
    @is_future = false
    #metodo per capire se l'evento è passato o futuro. Serve per visualizzare due show diversi in base alla data di oggi
    
    data_parsata = @event.datetime - 2.hours #2 ore per il fuso_orario
    if data_parsata.future?
        @is_future = true 
    end
  end

  def index
  end
  
  def search
    eventi_totali = Event.search(params) #restituisce tutti gli eventi in base ai parametri di ricerca, senza il parametro del genere musicale
    @events_posters = Hash.new()
    eventi_totali.each do |event|
      @events_posters[event.id] = event.poster.url(:thumb)
    end

    
    #---------------------------------------------------JOIN TRA EVENTI, BAND E GENERI DELLE BAND---------------------------------------------------------------
    if !params[:type_of_music].blank? #viene fatto SOLO se c'è il parametro. Così non spreco tempo nella ricerca degli altri campi che sono tutti sulla tabella 'events'
        genre_id = params[:type_of_music].to_i
        eventi_totali = eventi_totali.search_genre(genre_id)
    end 
   
   
        
        #preparazione dell'array che verrà passato al javascript con gli eventi splittati per la visualizzazione diversa dei pin
        eventi_live = eventi_totali.search_events_live
        eventi_futuri_participating = eventi_totali.search_events_participating(current_user) - eventi_live #gli eventi selezionati nella ricerca, a cui l'utente parteciperà
        eventi_passati_participated = eventi_totali.search_events_participated(current_user) - eventi_live
        eventi_futuri =  eventi_totali.search_all_future - eventi_futuri_participating - eventi_live
        eventi_passati = eventi_totali.search_all_past - eventi_passati_participated - eventi_live
        
        @events_array = [eventi_futuri, eventi_passati, eventi_futuri_participating, eventi_passati_participated, eventi_live].to_a.to_json.html_safe #vettore che passo al javascript
    
    
    @vicino_lat = params[:vicino_lat]
    @vicino_lon = params[:vicino_lon]

    respond_to do |format|
      format.html
      format.js 
    end
  end
  
  
  private
  
  def correct_user #controllo id utente = current_user
    @user = User.find(Event.find(params[:id]).user_id)
    if !current_user?(@user) #controllo id utente = current_user
      redirect_to root_path
    end
  end
  
end
