class CommentsController < ApplicationController
  before_filter :signed_in_user
  
  def create
    @event = Event.find_by_id(Integer(params[:event_id]))
    if @event.comment(params[:content],current_user.id)
      @comment = @event.comments.where(user_id:current_user.id).order('created_at DESC').first
      respond_to do |format|
        format.html {redirect_to request.referer}
        format.js
      end
    end
  end

  def destroy
    @comment = Comment.find_by_id(params[:id])
    @event = Event.find_by_id(@comment.event_id)
    if @comment.destroy
      respond_to do |format|
        format.html {redirect_to request.referer}
        format.js
      end
    end
  end
end
