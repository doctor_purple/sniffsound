class PagesController < ApplicationController
  require 'will_paginate/array'
  def home
    if signed_in?
      redirect_to '/dashboard'
    end
  end

  def dashboard
    
    if(signed_in?)
      @events = Event.search_all_future.paginate(page: params[:page], per_page:10)
      @events_most_popular = Event.search_most_popular #restituisce i primi 10 eventi più partecipati
      
      @events_posters = Hash.new()
      @events.each do |event|
      @events_posters[event.id] = event.poster.url(:thumb)
    end

      
      #-----------------query diversificate per la mappa della dashboard----------------------------
      events_next = Event.search_dashboard(false) - current_user.participating_events - Event.search_events_live
      events_past = Event.search_dashboard(true) - current_user.participating_events- Event.search_events_live
      events_participating_next = current_user.participating_events.search_dashboard(false)
      events_participating_past = current_user.participating_events.search_dashboard(true)
      events_live = Event.search_events_live
      @events_posters = @events_posters.to_json.html_safe
      @events_array = [events_next, events_past, events_participating_next, events_participating_past, events_live].to_a.to_json.html_safe
    else
      redirect_to root_path
    end
    
  end

  def explore
    
    respond_to do |format|
      format.html
      format.js
    end
    
  end
  

  def about
  end
  
  def search #metodo che gestisce la ricerca di un unico campo
      @users = User.search(params).paginate(page: params[:page])
      @bands = Band.search(params).paginate(page: params[:page])
      @events = Event.search_name(params[:search]).paginate(page: params[:page])   
      
      @tot = @users.length + @bands.length + @events.length
  end

end
