class UserEventRelationshipsController < ApplicationController
  before_filter :signed_in_user
  
  def create
    @event = Event.find(params[:user_event_relationship][:event_id])
    @user = current_user
    @user.participate!(@event)
    respond_to do |format|
      format.html { redirect_to '/dashboard' }
      format.js
    end
  end

  def destroy
    @user_event_relationship = UserEventRelationship.find(params[:id])
    #@event = Event.find(@user_event_relationship.event_id)
    @event = @user_event_relationship.event
    current_user.unparticipate!(@event)
    respond_to do |format|
      format.html { redirect_to '/dashboard' }
      format.js
    end
  end

  def edit
  end
end
