class BandsController < ApplicationController
  before_filter :signed_in_user, only: [:new,:create]
  
  
  def new
    @band = Band.new
    @users = User.all.to_json(only:[:id,:fullname]).to_s.html_safe;
  end
  
  def create
    @band = Band.new(params[:band])
    @band.user_band_relationships.build(user_id:current_user.id,priviledges:1,role:params[:role])
    @band.build_members(params,current_user.id)
    if @band.save
      @band.music_genre(params)
      flash[:success] = "Band creata correttamente!"
      redirect_to band_path(@band)
    else
      render 'new'
    end
  end  
  
  def edit
    @band = Band.find(params[:id])
    users = User.all
    @clean_users = users.to_json(only:[:id,:fullname]).to_s.html_safe
  end
  
  def update
    @band = Band.find(params[:id])
    @band.build_members(params,current_user.id)
    @band.update_attributes_and_members(params)
    redirect_to band_path(@band)
  end
  
  def show
    @band = Band.find(params[:id])
    @band_events = @band.participating_events
    @band_future_events = @band_events.search_all_future
    @band_past_events = @band_events.search_all_past
    @band_live_events = @band_events.search_events_live
    @total_audience = @band_events.count(group: :user).length
  end
  
  def destroy
    band = Band.find(params[:id])
    url = request.referer
    if("#{request.referer}/edit".include?(edit_band_path(band)))
      url = root_path
    end
    
    band.destroy
    
    redirect_to url
  end 
end
