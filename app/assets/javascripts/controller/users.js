//c'è da fare il validate anche sul form di classe .edit_user che viene richiamato dal "settings"

$(document).ready(function(){
	
	$("#new_user, .edit_user").validate({
		errorElement: "div",
		errorClass: "alert-error",

		rules: {
			"user[fullname]": {required: true, rangelength: [5, 20]},
			"user[email]": {required: true, email: true},
			"user[password]": {required: true, minlength: 6},
			"user[password_confirmation]": {required: true, equalTo: "#user_password"}
		},
		messages: {
			"user[fullname]": {required: "campo obbligatorio", rangelength: "tra i 5 e i 20 caratteri"},
			"user[email]": {required: "campo obbligatorio", email: "email non valida"},
			"user[password]": {required: "campo obbligatorio", minlength: "almeno 6 caratteri"},
			"user[password_confirmation]": {required: "campo obbligatorio", equalTo: "non coincide con la password"}		
		}
	});

});