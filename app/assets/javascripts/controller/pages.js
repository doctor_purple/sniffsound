// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/


$(document).ready(function() {
	
	$('#loginModale').modal({
		show: false
	});

	$('.carousel').carousel({
		pause: false
	});
  
	//pausa e fa scomparire i controlli del carousel quando compare la slide del signup
	$('.carousel').bind(
	    'slid',
	    function() {
	      if ($('.active').children('#signup-div').length > 0) {
	        $('#homeCarousel').carousel('pause');
	        $('.carousel-control').hide(200);
	      } else {
	        $('.carousel-control').show(200);
	      }
	        
	    }
	);
	
	//espande e comprime la mappa della bacheca
	$('#map_expand').click(function(event) {
		if (event.target.id == "searchTextField")
			return;
		$('#map_expand i').toggleClass("icon-chevron-down").toggleClass("icon-chevron-up");
		var h = 200;
		var s = 'Espandi mappa';
		if ($(this).hasClass('unexpanded')) {
			h = 400;
			s = 'Comprimi mappa';
		}
    
		$('#map_canvas').animate(
			{height: h}, 
			500,
			function() {
        		// $('#map_expand small').html(s);
      			$('#map_expand').toggleClass('unexpanded');
      		}
      	);
	});

});