$(document).ready(function(){
	
	$("#new_band, .edit_band").validate({
		ignore:"hidden",
		errorElement: "div",
		errorClass: "alert-error",

		rules: {
			"band[name]": {required: true, rangelength: [3, 20]},
			"band[description]": {required: true, minlength: 10},
			"role": {required: true}
		},
		messages: {
			"band[name]": {required: "campo obbligatorio", rangelength: "tra i 3 e i 20 caratteri"},
			"band[description]": {required: "campo obbligatorio", minlength: "minimo 10 caratteri"},
			"role": {required: "campo obbligatorio"}
		}
	});

});