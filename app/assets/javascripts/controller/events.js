//c'è da fare il validate anche sul form di classe .edit_user che viene richiamato dal "settings"

$(document).ready(function(){
	
	$("#new_event, .edit_event").validate({
		ignore:"hidden",
		errorElement: "div",
		errorClass: "alert-error",

		rules: {
			"event[name]": {required: true, rangelength: [3, 120]},
			"event[description]": {required: true, minlength: 10},
			"event[lat]": {required: true},
			"event[lon]": {required: true}
		},
		messages: {
			"event[name]": {required: "campo obbligatorio", rangelength: "tra i 3 e i 20 caratteri"},
			"event[description]": {required: "campo obbligatorio", minlength: "minimo 10 caratteri"},
			"event[lat]": {required: "coordinate obbligatorie"},
			"event[lon]": {required: "coordinate obbligatorie."}
		}
	});

});