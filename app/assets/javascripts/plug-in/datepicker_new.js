//Script per il popup della data
$(function() {
	
	var startDate = new Date(1920, 01, 01);

	$('#dp_new').datepicker().on('changeDate', function(ev) {
		$('#alert').hide();
		startDate = new Date(ev.date);
		$('#giorno').html($('#dp_new').data('date'));
		$('#giorno').addClass("btn btn-info disabled");
		$('#giornoHidden').prop('value', $('#dp_new').data('date'));
		$('#dp_new').datepicker('hide');
	});
});
