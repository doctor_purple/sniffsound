function initialize() {
   	
      	var lat = $('#event_lat').prop('value');	
		var lon = $('#event_lon').prop('value');
		
		var pos = new google.maps.LatLng(lat,lon);
		
        var centroMappa = new google.maps.LatLng(45.078854, 7.681389); //centro in Torino
     	var markers = [];
      	var pin = "/assets/pin.png";
      	
        var myOptions = {
				center: centroMappa,
				zoom: 12,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};

        var map = new google.maps.Map(document.getElementById('map-canvas-edit_event'),myOptions);
        map.setCenter(pos);
        addMarker(pos);
        
        var ricerca = document.getElementById('searchTextField');
        var autocomplete = new google.maps.places.Autocomplete(ricerca);
        autocomplete.bindTo('bounds', map);
        

		//funzione richiamata quando ce una ricerca nel text field
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
        	
			//prima cosa resetto tutti i marker sulla mappa
			setAllMap(null);
			markers = [];
			
			var place = autocomplete.getPlace();
			if (place.geometry.viewport) {
            	map.fitBounds(place.geometry.viewport);
			} else {
            	map.setCenter(place.geometry.location);
			}
			marker = new google.maps.Marker({
				animation:google.maps.Animation.BOUNCE,
				draggable:true,
				map : map,
				icon:pin
			});			
          
			marker.setPosition(place.geometry.location);
			
			google.maps.event.addListener(marker, 'dragend', function() {
		    	$('#event_lat').prop('value',this.position.lat());
				$('#event_lon').prop('value',this.position.lng());
			});
			
			$('#event_lat').prop('value',place.geometry.location.lat());
			$('#event_lon').prop('value',place.geometry.location.lng());
			
			//aggiungo il marker al vettore
			markers.push(marker);
			
			
          var address = '';
          if (place.address_components) {
            address = [(place.address_components[0] &&
                        place.address_components[0].short_name || ''),
                       (place.address_components[1] &&
                        place.address_components[1].short_name || ''),
                       (place.address_components[2] &&
                        place.address_components[2].short_name || '')
                      ].join(' ');
          }

        });
        
	      // Add a marker to the map and push to the array.
	      function addMarker(location) {
	      	setAllMap(null);
		    markers = [];
		    
		    marker = new google.maps.Marker({
				animation:google.maps.Animation.BOUNCE,
				position : location,
				draggable:true,
				map : map,
				icon:pin
			});
			
		    markers.push(marker);
		    
		    google.maps.event.addListener(marker, 'dragend', function() {
		    	$('#event_lat').prop('value',this.position.lat());
				$('#event_lon').prop('value',this.position.lng());
			});
	       
	     	$('#event_lat').prop('value',location.lat());
			$('#event_lon').prop('value',location.lng());
	      }
		    //setta la mappa ai marker, nel nostro caso uno  
			function setAllMap(map) {
		        for (var i = 0; i < markers.length; i++) {
		          markers[i].setMap(map);
		        }
		      }
		    //nasconde il marker sulla mappa
			$("#nascondi").click(function() {
			  setAllMap(null);
			});
			//mostra il marker sulla mappa
			$("#mostra").click(function() {
			  setAllMap(map);
			});
			
			google.maps.event.addListener(map, 'click', function(event) {
				addMarker(event.latLng);
	        });
		}
google.maps.event.addDomListener(window, 'load', initialize);
		

function blocca_enter(ev) {
	var key;
	if(window.event)
		key = window.event.keyCode;
	//internet explorer
	else
		key = ev.which;
	return key != 13;
}
