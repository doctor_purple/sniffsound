$(document).ready(function() {
	$("a:contains('Registra Band')").parent().addClass("active");
});
			var arrayNomi = new Array(); //array dei nomi di tuti gli utenti
			var i = 0;
			var current_member = 0; //indice dell'ultimo membro della band aggiunto
			
			
			//riempio l'array dei nomi
			$(all_users).each(function(){
				arrayNomi.push(this.fullname+"§"+this.id);
			});
			
			//plugin typeahead sul primo membro della band
			$("#member_"+current_member+"_name").typeahead({
				source: arrayNomi,
				member_id: current_member
			});
			
			//quando clicco per aggiungere un membro in più
			$(".aggiungi_membro").click(function(){

				//aggiungo una riga di html per il nuovo membro
				current_member++;
				$("#togli_membro_"+(current_member-1))
					.after("<a href='#' class='togli_membro btn btn-danger' id='togli_membro_"+current_member+"'>Remove</a>")
					.after("<input id='member_"+current_member+"_role' name='member["+current_member+"][role]' placeholder='Es. cantante, chitarrista...' class='user_role' type='text' />")
					.after("<input id='member_"+current_member+"_id' name='member["+current_member+"][id]' type='hidden' />")
					.after("<input id='member_"+current_member+"_name' name='member["+current_member+"][name]' class='user_autocompletion' data-provide='typeahead' placeholder='Cerca tra gli utenti..' type='text' autocomplete= 'off' />");
				//plugin-tipeahead per il nuovo membro
				$("#member_"+current_member+"_name").typeahead({
					source: arrayNomi,
					member_id: current_member
				});
				// console.log("aggiungo il membro: " + current_member);
				
				//listener sul click del link per rimuovere un elemento della band
				$("#togli_membro_"+current_member).click(function(){
					//non posso rimuovere la casellina per il primo membro
					if (current_member == 0)
						return;
					var membro_tolto = $(this).attr("id").split("_")[2];
					// console.log("tolgo il membro: " + membro_tolto);
					var tot_membri = $(".user_autocompletion").length;
					
					//rimuovo il markup html
					$("#member_"+membro_tolto+"_role").remove();
					$("#member_"+membro_tolto+"_id").remove();
					$("#member_"+membro_tolto+"_name").remove();
					$(this).remove();
					
					//riscalo tutti gli altri campi con gli indici giusti
					membro_tolto = parseInt(membro_tolto);
					for (var j = membro_tolto; j<(tot_membri-1); j++) {
						$("#member_"+(j+1)+"_role").attr("id", "member_"+j+"_role" ).attr("name", "member["+j+"][role]" );
						$("#member_"+(j+1)+"_id").attr("id", "member_"+j+"_id" ).attr("name", "member["+j+"][id]" );
						$("#member_"+(j+1)+"_name").attr("id", "member_"+j+"_name" ).attr("name", "member["+j+"][name]" );
						$("#togli_membro_"+(j+1)).attr("id", "togli_membro_"+j);
					}
					//porto il current member a valore giusto
					current_member = $(".user_autocompletion").length-1;
				});
			});
			
			$("#togli_membro_0").click(function(){
					//non posso rimuovere la casellina per il primo membro
					if (current_member == 0)
						return;
					var membro_tolto = $(this).attr("id").split("_")[2];
					// console.log("tolgo il membro: " + membro_tolto);
					var tot_membri = $(".user_autocompletion").length;
					
					//rimuovo il markup html
					$("#member_"+membro_tolto+"_role").remove();
					$("#member_"+membro_tolto+"_id").remove();
					$("#member_"+membro_tolto+"_name").remove();
					$(this).remove();
					
					//riscalo tutti gli altri campi con gli indici giusti
					membro_tolto = parseInt(membro_tolto);
					for (var j = membro_tolto; j<(tot_membri-1); j++) {
						$("#member_"+(j+1)+"_role").attr("id", "member_"+j+"_role" ).attr("name", "member["+j+"][role]" );
						$("#member_"+(j+1)+"_id").attr("id", "member_"+j+"_id" ).attr("name", "member["+j+"][id]" );
						$("#member_"+(j+1)+"_name").attr("id", "member_"+j+"_name" ).attr("name", "member["+j+"][name]" );
						$("#togli_membro_"+(j+1)).attr("id", "togli_membro_"+j);
					}
					//porto il current member a valore giusto
					current_member = $(".user_autocompletion").length-1;
			});
