$(document).ready(function() {
	settaAltezzaAside();
 	$(window).resize(function() {
  		settaAltezzaAside();
 	});
 	$('ul.nav-tabs li a').click(function() {
		window.setTimeout('settaAltezzaAside()',300);
	});
});

function settaAltezzaAside() {
 //setto l'altezza giusta della colonna laterale in base a quanti eventi ci sono in dashboard
 var h1 = $("aside.span3").height();
 var h = $(".row > .span7.offset1").height()-50;
 if ($(window).width()>767 && h>h1) {
  $("aside.span3").height(h);
 } else {
  $("aside.span3").css("height", "auto");
 }
}