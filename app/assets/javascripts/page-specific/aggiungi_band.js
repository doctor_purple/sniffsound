//i 2 array delle band esistenti (dx) e partecipanti (sx) all'evento
var colSx = new Array();
var colDx = new Array();

//costruttore dell'oggetto Band
function band(name, id) {
	this.name = name;
	this.id = id;
	this.stampa_li = function() {
		return "<li class='btn' id='"+this.id+"'>"+this.name+"</li>"
	}
} 

//metodo per ordinare 2 band alfabeticamente per nome
function sortBands(a, b) {
	return a.name <= b.name ? -1 : 1; 
}

//carica le bande nell'elenco di destra
function riempiElencoBande() {
	
	$(bands_array).each(function(){
		colDx.push(new band(this.name, this.id));
	});
	
	if (array_partecipanti != null) {
		$(array_partecipanti).each(function() {
			colSx.push(new band(this.name, this.id));
		});
	}
	
	//ordino la colonna di destra (quella di sinistra viene creata con gli elementi già in ordine)
	colDx.sort(sortBands);
	
	$("#elenco_completo ul").html(""); //rimuovo tutti gli elementi che c'erano e rimetto l'elenco
	
	//html per l'elenco di destra
	for (var i=0; i<colDx.length; i++) {
		$punto_elenco = $(colDx[i].stampa_li());
		$punto_elenco.addClass("btn-inverse");
		$("#elenco_completo ul").append($punto_elenco);
		$("#new_event input[type='submit'],form.edit_event input[type='submit']")
			.before("<input id='bands["+colDx[i].id+"]' name='bands["+colDx[i].id+"]' type='hidden' value='0'/>");
	}
	
	//html per l'elenco di sinistra
	for (var i=0; i<colSx.length; i++) {
		$punto_elenco = $(colSx[i].stampa_li());
		$punto_elenco.addClass("btn-warning");
		$("#partecipanti ul").append($punto_elenco);
		$("#new_event input[type='submit'],form.edit_event input[type='submit']")
			.before("<input id='bands["+colSx[i].id+"]' name='bands["+colSx[i].id+"]' type='hidden' value='1'/>");
	}
}

//mette il placeholder sono se non ci sono band nell'elenco
function controllaPlaceholder() {
	if ($("#partecipanti ul li").size() == 0)
		$("#partecipanti ul").append("<li class='placeholder'>Trascina le band qui o cliccaci sopra...</li>");
}

//restituisce l'indice dell'elemento all'interno dell'array; se non esiste, ritorna -1
function trovaElementoInArray(el, array) {
	var i = 0;
	for (i=0; i<array.length; i++)
		if (array[i].name == el.name) 
			return i;
	return -1;
}

//gestisce il click sugli elementi per spostarli da una colonna all'altra
function gestisci_click() {
	//tolgo l'event listener sul click e lo rimetto "da zero"
	$(".elenco_band ul li").unbind("click");
	$(".elenco_band ul li").click(function() {
		var id_lato = $(this).parent().parent(".elenco_band").attr("id");
		
		if (id_lato === "elenco_completo") { //se sto spostando da dx a sx
			//rimuovo il placeholder nel caso ci fosse 
			$("#partecipanti").find( "ul .placeholder" ).remove();
			//rimuovo la band nell'elenco da cui la sto prendendo
			$("#elenco_completo li:contains('"+$(this).text()+"')").remove();
			//creo un nuovo oggetto band per la band che sto spostando e la cerco all'interno dell'array
			var h = new band($(this).text(), $(this).attr("id"));
			var index = trovaElementoInArray(h, colDx);
				
			if (index > -1)
				spostaInColSx(colDx[index]);
			
		} else { //se sto spostando da sx a dx
			//rimuovo la band nell'elenco da cui la sto prendendo
			$("#partecipanti li:contains('"+$(this).text()+"')").remove();
			//creo un nuovo oggetto band per la band che sto spostando e la cerco all'interno dell'array
			var h = new band($(this).text(), $(this).attr("id"));
			var index = trovaElementoInArray(h, colSx);
				
			if (index > -1)
				spostaInColDx(colSx[index]);
		
			//eventualmente rimetto il placeholder e "resetto" il filtro
			controllaPlaceholder();
			$("#band_search").val("");
		}	
	});
}

//chiamato quando si dragga da dx a sx
function spostaInColSx(band) {
	colSx.push(band);
	colSx.sort(sortBands);
	var i = 0;
	var index;
	
	//ristampo il vettore della colonna di sinistra
	$("#partecipanti ul").html("");
	
	for (i=0; i<colSx.length; i++ ) {
		$punto_elenco = $(colSx[i].stampa_li());
		$punto_elenco.addClass("btn-warning");
		$punto_elenco.draggable({revert:"invalid", appendTo: 'body', helper: "clone", scroll: false}).appendTo("#partecipanti ul");
	}
	
	//elimino l'elemento dalla colonna destra
	index = trovaElementoInArray(band, colDx);
	if (index > -1)
		colDx.splice(index,1);
		
	//cambio il value nel input hidden associato, mettendolo a 1 = la band partecipa all'evento
	$("input[name='bands["+band.id+"]']").val("1");
	
	gestisci_click();
}

//chiamato quando si dragga da sx a dx
function spostaInColDx(band) {
	colDx.push(band);
	colDx.sort(sortBands);
	var i = 0;
	var index;
	
	//ristampo il vettore della colonna Dx
	$("#elenco_completo ul").html("");
	for (i=0; i<colDx.length; i++ ) {
		$punto_elenco = $(colDx[i].stampa_li());
		$punto_elenco.addClass("btn-inverse");
		$punto_elenco.draggable({revert:"invalid", appendTo: 'body', helper: "clone", scroll: false}).appendTo("#elenco_completo ul");
	}
	
	//elimino l'elemento dalla colonna sinistra
	index = trovaElementoInArray(band, colSx);
	if (index > -1)
		colSx.splice(index,1);	
		
	//cambio il value nel input hidden associato, mettendolo a 0 = la band non partecipa all'evento
	$("input[name='bands["+band.id+"]']").val("0");
	
	gestisci_click();
}

	
$(document).ready(function() {
	
	//setup iniziale
	riempiElencoBande();
	controllaPlaceholder();
	gestisci_click();
	$("a:contains('Crea Evento')").parent().addClass("active");
	
	//DRAGGABLE
	$( "li:not(.placeholder)", '.elenco_band ul'  ).draggable({
		revert: "invalid",
		appendTo: 'body',
		helper: "clone",
		scroll: false
	});
	//DROP NELLA COLONNA DI SINISTRA
	$( "#partecipanti").droppable({
		accept: "#elenco_completo li",
		hoverClass: "ui-state-hover-sx",
		drop: function( event, ui ) {
			//rimuovo il placeholder
			$(this).find( "ul .placeholder" ).remove();
			//rimuovo la band dall'altra parte
			$("#elenco_completo li:contains('"+ui.draggable.text()+"')").remove();
			//aggiungo un nuovo punto elenco draggable
			// $( "<li></li>" ).text( ui.draggable.text() ).draggable({revert:"invalid"}).attr("id", ui.draggable.attr("id")).appendTo( this );
			var h = new band(ui.draggable.text(), ui.draggable.attr("id"));
			var index = trovaElementoInArray(h, colDx);
				
			if (index > -1) {
				// alert("sposto"+ colDx[index].name +"in colSx!")
				spostaInColSx(colDx[index]);
			}
		}
	})
	
	//DROP NELLA COLONNA DI DESTRA
	$( "#elenco_completo").droppable({
		accept: "#partecipanti li",
		hoverClass: "ui-state-hover-dx",
		drop: function( event, ui ) {
			//rimuovo la band dall'altra parte
			$("#partecipanti li:contains('"+ui.draggable.text()+"')").remove();
			//aggiungo un nuovo punto elenco draggable
			// $( "<li></li>" ).text( ui.draggable.text() ).draggable({revert:"invalid"}).attr("id", ui.draggable.attr("id")).appendTo( this );
			var h = new band(ui.draggable.text(), ui.draggable.attr("id"));
			var index = trovaElementoInArray(h, colSx);
				
			if (index > -1)
				spostaInColDx(colSx[index]);
		
			controllaPlaceholder();
			$("#band_search").val("");
		}
	})
	
	$("#band_search").keyup(function() {
		var value = $(this).val();
		var i = 0;
		//scorro il vettore delle band
		for (i=0; i<colDx.length; i++) {
			if (colDx[i].name.toLowerCase().indexOf(value.toLowerCase()) == -1) {
				//se non corrisponde ai risultati di ricerca, nascondo il risultato
				$("#"+colDx[i].id).hide();
			} else {
				//altrimenti lo mostro
				$("#"+colDx[i].id).show();
			}
		}
	}).keyup();
});