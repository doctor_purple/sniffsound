function updateDistanceSlider(){
	function toRad(value) {
 	return value * Math.PI / 180;
	}
	//Formula di Haversine
 	var R = 6371; // raggio terrestre km
	var dLat = toRad(parseFloat($("#event_range_selector input").val()));
	var dLon = toRad(parseFloat($("#event_range_selector input").val()));
	var lat1 = toRad(parseFloat($("#vicino_lat").val()));
	var lat2 = toRad(parseFloat($("#vicino_lat").val())+parseFloat($("#event_range_selector input").val()));
	
	var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
	        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
	var d = R * c;

	var displayed_distance = "Select Location";
	if($("#vicino_lat").val().length!=0) displayed_distance = "Km: "+parseInt(d);
 	$("#event_range_selector input").next("label").text(displayed_distance);
}
 
$(document).ready(function() {
 $("a.btn-navbar").click(function() {
  $("#map_canvas_explore").toggleClass("below_the_line");
 });
 
 $("a:contains('Eventi')").parent().addClass("active");
 
 //Distance range slider
 $("#event_range_selector input").change(updateDistanceSlider);
});

function initialize() {
      	//inizialize per impostare una mappa di default senza marker

      	
      	var dragging = false;
      	var centroMappa = new google.maps.LatLng(45.078854, 7.681389); //mappa centrata a Torino
      	
		var myOptions = {
			center: centroMappa,
			zoom: 10,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
         
		var map = new google.maps.Map(document.getElementById("map_canvas_explore"), myOptions);
		
		//modifica dello stile della mappa di default
		var array_stile_mappa = [
				{//tutte le scritte blu
				featureType: "all",
			  	elementType:"labels",
			    stylers: [
			      { hue: "#007FFF" },
			      { saturation: 80 },
  				  { lightness: 10 }
			    ]
			  },{//tutto lo sfondo leggermente arancione
			  	featureType: "all",
			  	elementType:"geometry",
			    stylers: [
			      { hue: "#EE9900" },
			      { saturation: 1 },
  				  { lightness: -5 }
			    ]
			  },{//tutte le strutture arancioni
			    featureType: "landscape.man_made",
			    stylers: [
			      { hue: "#EE9900" },
			      { saturation: 70 }
			    ]
			  },{//nessuna scritta per i luoghi di business
			    featureType: "poi.business",
			    elementType: "labels",
			    stylers: [
			      { visibility: "off" }
			    ]
			  },
			  {//nessuna scritta per le strade
			    featureType: "road.arterial",
			    elementType: "labels",
			    stylers: [
			      { visibility: "off" }
			    ]
			  }
			];
			
			map.setOptions({styles: array_stile_mappa});
							
			//riposizionare la mappa centrata
			google.maps.event.addListener(map, 'bounds_changed', function() {
                if(!dragging)
                	map.setCenter(centroMappa);
			});	
			google.maps.event.addListener(map, 'dragstart', function() {
        		dragging = true;
    		});
			google.maps.event.addListener(map, 'dragend', function() {
				centroMappa = map.getCenter();
        		dragging = false;
			});
			
			var ricerca = document.getElementById('near_to');
			var autocomplete = new google.maps.places.Autocomplete(ricerca);
			autocomplete.bindTo('bounds', map);
	        	
			//funzione richiamata quando ce una ricerca nel text field Vicino A
			google.maps.event.addListener(autocomplete, 'place_changed', function() {
				
				var place = autocomplete.getPlace();
				
				//memorizza i valori nei due campi hidden da passare alla ricerca
				$('#vicino_lat').prop('value',place.geometry.location.lat());
				$('#vicino_lon').prop('value',place.geometry.location.lng());
					
				var address = '';
				if (place.address_components) {
					address = [(place.address_components[0] &&
			                    place.address_components[0].short_name || ''),
			                   (place.address_components[1] &&
			                    place.address_components[1].short_name || ''),
			                    (place.address_components[2] &&
			                     place.address_components[2].short_name || '')
			                   ].join(' ');
				}
			});
			
			
			$('#form_filtri').keypress(function(event){
				if(event.keyCode == 13){
					event.cancelBubble == true;
					event.returnValue == false;
				}
			});
		}
		
google.maps.event.addDomListener(window, 'load', initialize);

function blocca_enter(ev){
	var key;
	if(window.event)
		key = window.event.keyCode;//internet explorer
	else
		key = ev.which;
	return key != 13;
}		

$(document).ready(function(){
			ridimensiona();
});
		
$(window).resize(function(){
	ridimensiona();
});
		
function ridimensiona() {
 var h_filtri = parseInt($('#filtri').height()) + parseInt($('#filtri').css('margin-top').substring(0,2));
 // if ($(window).width()<980)
  // h_filtri = h_filtri+46;
 // if ($(window).width()<887)
  // h_filtri = h_filtri+28;
 // if ($(window).width()<540)
  // h_filtri = h_filtri+28;
 var h_mappa = $(window).height() - h_filtri;
 var w = $(window).width();
 $('#debug').html(w);
 //ridimensionamento mappa e posizionamento
 $('#map_canvas_explore').height(h_mappa);
 $('#map_canvas_explore').css('top',h_filtri);
 
}
		
		function search_callback(){
			      				//Aggiungo le source delle immagini
			$(events_array).each(function(){
				$(this).each(function(){
					this.poster = $(events_posters)[0][this.id];
				});
			});

			
			//nel momento della ricerca crea la mappa con i marker ottenuti
			var events_next = events_array[0]; //array degli eventi dei prossimi 30 giorni
			var events_past = events_array[1]; //array degli eventi degli ultimi 30 giorni
			var events_next_me = events_array[2];//array degli eventi a cui parteciperà l'utente nei prossimi 30 giorni
			var events_past_me = events_array[3];//array degli eventi a cui ha partecipato l'utente negli ultimi 30 giorni
			var events_live = events_array[4] //array degli eventi live in questo momento
			
	      	var dragging = false;
	      	var centroMappa = new google.maps.LatLng(45.078854, 7.681389); //centro in Torino
	      	var pin = "/assets/pin.png"
	      	var pin_pass = "/assets/pin_pass.png";
	      	var pin_me = "/assets/pin_me.png";
	      	var pin_pass_me = "/assets/pin_pass_me.png";
	      	var pin_live = "/assets/pin_live.png";
			var infowindow = null;//per visualizzare solo una infoWindow

			var nuovoCentro = new google.maps.LatLng(vicino_lat,vicino_lon);
			if(nuovoCentro.lat()!=0 &&nuovoCentro.lng()!= 0){
				centroMappa = nuovoCentro;
			}
			
			var myOptions = {
				center: centroMappa,
				zoom: 10,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};
	         
			var map = new google.maps.Map(document.getElementById("map_canvas_explore"), myOptions);
		

			function placeMarker(location,titolo,contenuto,icona,live) {
				var marker = new google.maps.Marker({
					position : location,
					map : map,
					title: titolo,
					icon:icona,
					animation:google.maps.Animation.DROP
				});
				if(live){//se il concerto è live saltella appena inserito
					marker.setAnimation(google.maps.Animation.BOUNCE);
				}
	  			google.maps.event.addListener(marker, 'click', function() {
				    if (infowindow) {
				        infowindow.close();
				    }
				    infowindow = new google.maps.InfoWindow(
				      { content: contenuto,
				        size: new google.maps.Size(20,20)
				      });
				      infowindow.open(map,marker);
				});

	  			//creaInfoWindow(marker,contenuto)

				google.maps.event.addListener(marker, 'mouseover', toggleBounce);
				google.maps.event.addListener(marker, 'mouseout', stopBounce);
			
				//QUANDO CE IL MOUSEOVER IL PIN SALTELLA
				function toggleBounce() {
					if (marker.getAnimation() != null) {
						marker.setAnimation(null);
					} else {
						marker.setAnimation(google.maps.Animation.BOUNCE);
					}
				}
				function stopBounce(){
					marker.setAnimation(null);
				}
			}	
			
			// function creaInfoWindow(marker,contenuto){	
				// var infowindow = new google.maps.InfoWindow(
			      // { content: contenuto,
			        // size: new google.maps.Size(20,20)
			      // });
	  			// google.maps.event.addListener(marker, 'click', function() {
	   				// infowindow.open(map,marker);
	  			// });
			// }
			
			//modifica dello stile della mappa di default
			var array_stile_mappa = [
					{//tutte le scritte blu
					featureType: "all",
				  	elementType:"labels",
				    stylers: [
				      { hue: "#007FFF" },
				      { saturation: 80 },
	  				  { lightness: 10 }
				    ]
				  },{//tutto lo sfondo leggermente arancione
				  	featureType: "all",
				  	elementType:"geometry",
				    stylers: [
				      { hue: "#EE9900" },
				      { saturation: 1 },
	  				  { lightness: -5 }
				    ]
				  },{//tutte le strutture arancioni
				    featureType: "landscape.man_made",
				    stylers: [
				      { hue: "#EE9900" },
				      { saturation: 70 }
				    ]
				  },{//nessuna scritta per i luoghi di business
				    featureType: "poi.business",
				    elementType: "labels",
				    stylers: [
				      { visibility: "off" }
				    ]
				  },
				  {//nessuna scritta per le strade
				    featureType: "road.arterial",
				    elementType: "labels",
				    stylers: [
				      { visibility: "off" }
				    ]
				  }
				];
				
				map.setOptions({styles: array_stile_mappa});
				
				//riposizionare la mappa centrata
				google.maps.event.addListener(map, 'bounds_changed', function() {
	                if(!dragging)
	                	map.setCenter(centroMappa);
				});	
				google.maps.event.addListener(map, 'dragstart', function() {
	        		dragging = true;
	    		});
				google.maps.event.addListener(map, 'dragend', function() {
					centroMappa = map.getCenter();
	        		dragging = false;
				});
				
				var ricerca = document.getElementById('near_to');
	        	var autocomplete = new google.maps.places.Autocomplete(ricerca);
	        	autocomplete.bindTo('bounds', map);
	        	
				//funzione richiamata quando ce una ricerca nel text field Vicino A
		        google.maps.event.addListener(autocomplete, 'place_changed', function() {
					
					var place = autocomplete.getPlace();
					
					//memorizza i valori nei due campi hidden da passare alla ricerca
					$('#vicino_lat').prop('value',place.geometry.location.lat());
					$('#vicino_lon').prop('value',place.geometry.location.lng());
					
					var address = '';
					if (place.address_components) {
						address = [(place.address_components[0] &&
			                        place.address_components[0].short_name || ''),
			                       (place.address_components[1] &&
			                        place.address_components[1].short_name || ''),
			                       (place.address_components[2] &&
			                        place.address_components[2].short_name || '')
			                      ].join(' ');
					}
		        });
		        
				//REIMPOSTATA LA MAPPA AGGIUNGO I MARKER
				
				var num_eventi = 0;
				function creaContenuto(foto,id, titolo, data){

						//splitting in data e ora
						date = data.split("T",2)[0];
						orario = data.split("T",2)[1];
					
						//subsplitting della data
						anno = date.split("-")[0];
						mese = date.split("-")[1];
						giorno = date.split("-")[2];
					
						//subsplitting della data
						ora = orario.split(":")[0];
						minuti = orario.split(":")[1];
					
						//stringa da rimandare
				return '<div class="vignetta"><img src="'+foto+'" alt="Locandina"/><div class="info"><p><a href="/events/'+id+'">'+titolo+'</a></p><p>Il '+giorno+'/'+mese+'/'+anno+' alle '+ora+':'+minuti+'</p></div>';
				}
				
			$(events_next).each(function(){
				var location = new google.maps.LatLng(this.lat, this.lon);
				var contenuto = creaContenuto(this.poster,this.id,this.name,this.datetime);
				placeMarker(location,this.name,contenuto,pin);
				num_eventi++;
			});
			//ogni event passato aggiungo il marker
			$(events_past).each(function(){
				var location = new google.maps.LatLng(this.lat, this.lon);
				var contenuto = creaContenuto(this.poster,this.id,this.name,this.datetime);
				placeMarker(location,this.name,contenuto,pin_pass);
				num_eventi++;
			});
			//ogni event futuro a cui parteciperò aggiungo il marker
			$(events_next_me).each(function(){
				var location = new google.maps.LatLng(this.lat, this.lon);
				var contenuto = creaContenuto(this.poster,this.id,this.name,this.datetime);
				placeMarker(location,this.name,contenuto,pin_me);
				num_eventi++;
			});
			//ogni event passato a cui ho partecipato aggiungo il marker
			$(events_past_me).each(function(){
				var location = new google.maps.LatLng(this.lat, this.lon);
				var contenuto = creaContenuto(this.poster,this.id,this.name,this.datetime);
				placeMarker(location,this.name,contenuto,pin_pass_me);
				num_eventi++;
			});
			//ogni event live che si sta svolgendo in questo momento
			$(events_live).each(function(){
				var location = new google.maps.LatLng(this.lat, this.lon);
				var contenuto = creaContenuto(this.poster,this.id,this.name,this.datetime);
				var live = true;
				placeMarker(location,this.name,contenuto,pin_live,live);
				num_eventi++;
			});
			
			alert("concerti trovati: " + num_eventi);
				
				
	}