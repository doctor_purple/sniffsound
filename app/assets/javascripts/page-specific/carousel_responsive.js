$(document).ready(function () {
	updateCarouselSize();
    $(window).resize(function() {
        updateCarouselSize();
    });
});

function updateCarouselSize() {
	var width = $(window).width();
	if (width > 1199) {
		$("#carousel_trova").attr("src", "/assets/carousel/mappa-1170.png");
		$("#carousel_condividi").attr("src", "/assets/carousel/condividi-1170.png");
		$("#carousel_rivivi").attr("src", "/assets/carousel/rivivi-1170.png");
	} else if (width>969) {
		$("#carousel_trova").attr("src", "/assets/carousel/mappa-940.png");
		$("#carousel_condividi").attr("src", "/assets/carousel/condividi-940.png");
		$("#carousel_rivivi").attr("src", "/assets/carousel/rivivi-940.png");
	}
}