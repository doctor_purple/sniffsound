function initialize() {
	// per settare il marker nel luogo dell evento
	var lat = $('#lat_hidden').prop('value');	
	var lon = $('#lon_hidden').prop('value');
	var centroMappa = new google.maps.LatLng(lat,lon);

	//var centroMappa = new google.maps.LatLng(45.078854, 7.681389); //centro in Torino	
	var pin = "/assets/pin.png";
	
	var myOptions = {
		center: centroMappa,
		zoom: 14,
		mapTypeId:google.maps.MapTypeId.ROADMAP
	}
	var map = new google.maps.Map(document.getElementById('mappa_evento'),myOptions);
	
	addMarker(centroMappa);

	// Aggiunta Marker alla mappa
	function addMarker(location) {
		marker = new google.maps.Marker({
			animation : google.maps.Animation.BOUNCE,
			position : location,
			map : map,
			icon : pin
		});
	}

}

google.maps.event.addDomListener(window, 'load', initialize);