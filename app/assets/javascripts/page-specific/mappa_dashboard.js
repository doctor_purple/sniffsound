$(document).ready(function() {
 settaAltezzaSidebar();
 $(window).resize(function() {
  settaAltezzaSidebar();
 });
});

function settaAltezzaSidebar() {
 //setto l'altezza giusta della colonna laterale in base a quanti eventi ci sono in dashboard
 var h = $(".elenco_news").height()-$(".elenco_news h2").height()-2*($(".elenco_news .pagination").height()+36)-11;
 if ($(window).width()>767) {
  $("#bacheca > aside").height(h);
 } else {
  $("#bacheca > aside").css("height", "auto");
 }
}

function initialize() {
			var events_array = parseJSON(); //questo è l'array generale

			//Aggiungo poster collegate agli id
			$(events_array).each(function(){
						$(this).each(function(){
							this.poster = $(events_posters)[0][this.id];
						});
			});

			var events_next = events_array[0]; //array degli eventi dei prossimi 30 giorni
			var events_past = events_array[1]; //array degli eventi degli ultimi 30 giorni
			var events_next_me = events_array[2];//array degli eventi a cui parteciperà l'utente nei prossimi 30 giorni
			var events_past_me = events_array[3];//array degli eventi a cui ha partecipato l'utente negli ultimi 30 giorni
			var events_live = events_array[4] //array degli eventi live in questo momento
			
			// alert(events_live.length);
			var infowindow = null;//per visualizzare solo una infoWindow
			var map;
			var centroMappa = new google.maps.LatLng(45.078854, 7.681389); //mappa centrata a Torino
	      	var pin = "/assets/pin.png";
	      	var pin_pass = "/assets/pin_pass.png";
	      	var pin_me = "/assets/pin_me.png";
	      	var pin_pass_me = "/assets/pin_pass_me.png";
	      	var pin_live = "/assets/pin_live.png"
	      	
	        var myOptions = {
	          center: centroMappa,//se non va la geolocalizzazione
	          zoom: 11,
	          mapTypeId: google.maps.MapTypeId.ROADMAP
	        };
	        map = new google.maps.Map(document.getElementById('map_canvas'),myOptions);
	
			var input = document.getElementById('searchTextField');
	        var autocomplete = new google.maps.places.Autocomplete(input);
	
	        autocomplete.bindTo('bounds', map);
	        google.maps.event.addListener(autocomplete, 'place_changed', function() {
	          var place = autocomplete.getPlace();
	          if (place.geometry.viewport) {
	            map.fitBounds(place.geometry.viewport);
	          } else {
	            map.setCenter(place.geometry.location);
	            map.setZoom(17);  // Why 17? Because it looks good.
	          }
	        });
	
			function placeMarker(location,titolo,contenuto,icona,live) {
				var marker = new google.maps.Marker({
					position : location,
					map : map,
					title: titolo,
					icon:icona,
					animation:google.maps.Animation.DROP
				});
				if(live){//se il concerto è live saltella appena inserito
					marker.setAnimation(google.maps.Animation.BOUNCE);
				}
				google.maps.event.addListener(marker, 'click', function() {
				    if (infowindow) {
				        infowindow.close();
				    }
				    infowindow = new google.maps.InfoWindow({
				    	content: contenuto,
				        size: new google.maps.Size(100,100)
				    });
				 	// google.maps.event.addListener(infowindow, 'domready', function() {
				      // alert($(('div').parent('div').parent('div').parent('.vignetta')));
				      // $(('div').parent('div').parent('div').parent('.vignetta').css("height","200px"));
					// });  
				    infowindow.open(map,marker);
				      
				});
				
				//creaInfoWindow(marker,contenuto);
				
				google.maps.event.addListener(marker, 'mouseover', toggleBounce);
				google.maps.event.addListener(marker, 'mouseout', stopBounce);
			
				//QUANDO CE IL MOUSEOVER IL PIN SALTELLA
				function toggleBounce() {
					if (marker.getAnimation() != null) {
						marker.setAnimation(null);
					} else {
						marker.setAnimation(google.maps.Animation.BOUNCE);
					}
				}
				function stopBounce(){
					marker.setAnimation(null);
				}
			}
			
	        // Try HTML5 geolocation
	        if(navigator.geolocation) {
	          navigator.geolocation.getCurrentPosition(function(position) {
	            var pos = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
	                                             
	            // var infowindow = new google.maps.InfoWindow({
	              // map: map,
	              // position: pos,
	              // content: 'Location found using HTML5.'
	            // });
	            map.setCenter(pos);//ricentra la mappa con la geolocalizzazione
	            
	          }, function() {
	            handleNoGeolocation(true);
	          });
	        } else {
	          // Browser doesn't support Geolocation
	          handleNoGeolocation(false);
	        }
	        
			//modifica dello stile della mappa di default
		  var array_stile_mappa = [
				{//tutte le scritte blu
				featureType: "all",
			  	elementType:"labels",
			    stylers: [
			      { hue: "#007FFF" },
			      { saturation: 80 },
  				  { lightness: 10 }
			    ]
			  },{//tutto lo sfondo leggermente arancione
			  	featureType: "all",
			  	elementType:"geometry",
			    stylers: [
			      { hue: "#EE9900" },
			      { saturation: 1 },
  				  { lightness: -5 }
			    ]
			  },{//tutte le strutture arancioni
			    featureType: "landscape.man_made",
			    stylers: [
			      { hue: "#EE9900" },
			      { saturation: 70 }
			    ]
			  },{//nessuna scritta per i luoghi di business
			    featureType: "poi.business",
			    elementType: "labels",
			    stylers: [
			      { visibility: "off" }
			    ]
			  },
			  {//nessuna scritta per le strade
			    featureType: "road.arterial",
			    elementType: "labels",
			    stylers: [
			      { visibility: "off" }
			    ]
			  }
			];
			
			map.setOptions({styles: array_stile_mappa});
			
			//AGGIUNGO I MARKER
			var num_eventi = 0;
			function creaContenuto(foto,id, titolo, data){

					//splitting in data e ora
					date = data.split("T",2)[0];
					orario = data.split("T",2)[1];
				
					//subsplitting della data
					anno = date.split("-")[0];
					mese = date.split("-")[1];
					giorno = date.split("-")[2];
				
					//subsplitting della data
					ora = orario.split(":")[0];
					minuti = orario.split(":")[1];
				
					//stringa da rimandare
			return '<div class="vignetta"><img src="'+foto+'" alt="Locandina"/><div class="info"><p><a href="/events/'+id+'">'+titolo+'</a></p><p>Il '+giorno+'/'+mese+'/'+anno+' alle '+ora+':'+minuti+'</p></div>';
			}
			//ogni event futuro aggiungo il marker
			$(events_next).each(function(){
				var location = new google.maps.LatLng(this.lat, this.lon);
				var contenuto = creaContenuto(this.poster,this.id,this.name,this.datetime);
				placeMarker(location,this.name,contenuto,pin);
				num_eventi++;
			});
			//ogni event passato aggiungo il marker
			$(events_past).each(function(){
				var location = new google.maps.LatLng(this.lat, this.lon);
				var contenuto = creaContenuto(this.poster,this.id,this.name,this.datetime);
				placeMarker(location,this.name,contenuto,pin_pass);
				num_eventi++;
			});
			//ogni event futuro a cui parteciperò aggiungo il marker
			$(events_next_me).each(function(){
				var location = new google.maps.LatLng(this.lat, this.lon);
				var contenuto = creaContenuto(this.poster,this.id,this.name,this.datetime);
				placeMarker(location,this.name,contenuto,pin_me);
				num_eventi++;
			});
			//ogni event passato a cui ho partecipato aggiungo il marker
			$(events_past_me).each(function(){
				var location = new google.maps.LatLng(this.lat, this.lon);
				var contenuto = creaContenuto(this.poster,this.id,this.name,this.datetime);
				placeMarker(location,this.name,contenuto,pin_pass_me);
				num_eventi++;
			});
			//ogni event live che si sta svolgendo in questo momento
			$(events_live).each(function(){
				var location = new google.maps.LatLng(this.lat, this.lon);
				var contenuto = creaContenuto(this.poster,this.id,this.name,this.datetime);
				var live = true;
				placeMarker(location,this.name,contenuto,pin_live,live);
				num_eventi++;
			});
			
}

		  
function handleNoGeolocation(errorFlag) {
	if (errorFlag) {
		var content = 'Error: The Geolocation service failed.';
	} else {
		var content = 'Error: Your browser doesn\'t support geolocation.';
	}

	var options = {
		map : map,
		position : new google.maps.LatLng(60, 105),
		content : content
	};

	var infowindow = new google.maps.InfoWindow(options);
	map.setCenter(options.position);
}

google.maps.event.addDomListener(window, 'load', initialize);

function blocca_enter(ev){
	var key;
	if(window.event)
		key = window.event.keyCode;//internet explorer
	else
		key = ev.which;
	return key != 13;
}
