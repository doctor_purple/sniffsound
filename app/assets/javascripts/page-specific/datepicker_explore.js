//Script per il popup della data
		$(function(){
			window.prettyPrint && prettyPrint();
			
			//intervallo iniziale molto ampio dal 1920 al 2100
			var startDate = new Date(1920,01,01);
			var endDate = new Date(2100,01,01);
			
			$('#dp1').datepicker()
				.on('changeDate', function(ev){
					if (ev.date.valueOf() > endDate.valueOf()){
						alert("La data iniziale non può essere successiva alla data finale");
						$('#alert').show().find('strong').text('The start date can not be greater then the end date');
					} else {
						$('#alert').hide();
						startDate = new Date(ev.date);
						$('#startDate').html($('#dp1').data('date'));
						$('#startDate').addClass("btn btn-info disabled");
						$('#startDateHidden').prop('value', $('#dp1').data('date'));
					}
					$('#dp1').datepicker('hide');
				});
			$('#dp2').datepicker()
				.on('changeDate', function(ev){
					if (ev.date.valueOf() < startDate.valueOf()){
						alert("La data finale non può essere precedente alla data iniziale");
						$('#alert').show().find('strong').text('The end date can not be less then the start date');
					} else {
						$('#alert').hide();
						endDate = new Date(ev.date);
						$('#endDate').html($('#dp2').data('date'));
						$('#endDate').addClass("btn btn-info disabled");
						$('#endDateHidden').prop('value',$('#dp2').data('date'))
					}
					$('#dp2').datepicker('hide');
				});
		});