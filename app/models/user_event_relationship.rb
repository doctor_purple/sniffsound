class UserEventRelationship < ActiveRecord::Base
  attr_accessible :state,:user_id,:event_id
  
  belongs_to :user
  belongs_to :event
  
end
