class Band < ActiveRecord::Base

  attr_accessible :description, :name
  
  has_many :event_band_relationships, dependent: :destroy
  has_many :participating_events, through: :event_band_relationships, source: :event
  has_many :band_genre_relationships, dependent: :destroy
  has_many :music_genres, through: :band_genre_relationships, source: :genre

  validates :name, presence: true,
                   uniqueness: { case_sensitive: false,
                                 message: "nome in uso" }
  has_many :user_band_relationships, dependent: :destroy
  has_many :members, through: :user_band_relationships, source: :user
  has_many :fan_band_relationships, dependent: :destroy
  has_many :fans, through: :fan_band_relationships, source: :user
 #metodi per aiutarsi a reperire dati utili
 
 def administrator_role
   self.user_band_relationships.find_by_user_id(self.administrator.id).role
 end
 
 def administrator
   User.find_by_id(self.user_band_relationships.find_by_priviledges(true).user_id)
 end
 def administrator?(user)
   self.administrator==user
 end
 #aggiornamento band
 
 def update_attributes_and_members(params)
   self.update_attributes(params[:band])
   #Eliminazione dei membri
   if not params[:delete_member].nil?
     params[:delete_member].each do |k,v|
       if not administrator?(User.find_by_id(k))
        self.user_band_relationships.find_by_user_id(k).destroy
       end
     end
   end
   #Ruolo dei membri
   self.user_band_relationships.find_by_user_id(self.administrator.id).update_attributes(role:params[:role])#amministratore
   if not params[:member_role].nil?
     params[:member_role].each do |k,v| #altri utenti
       if !self.user_band_relationships.find_by_user_id(k).nil? && v!=self.user_band_relationships.find_by_user_id(k).role
         self.user_band_relationships.find_by_user_id(k).update_attributes(role:v)
       end
     end
   end
   #Generi
   if not params[:genre].nil?
     if not self.band_genre_relationships.nil? #Tolgo le relazioni coi generi precedenti
       self.band_genre_relationships.all.each do |relationship|
         relationship.destroy
       end
     end
     params[:genre].each do |k,v|
       if (Integer(v) == 1)
         self.band_genre_relationships.create(genre_id:k)
       end
     end
   end
 end
 
 #controllo che i membri aggiunti siano corretti
 def build_members(params,current_user_id)
   if not params[:member].nil?
      params[:member].each do |k,v|
        if((!User.find_by_id(v[:id]).nil?) && User.find_by_id(v[:id]).fullname==v[:name] && Integer(current_user_id)!=Integer(v[:id]))
          Rails.logger.info("#{current_user_id} uguale a #{v[:id]}")
         self.user_band_relationships.find_or_initialize_by_user_id(user_id: v[:id], role: v[:role])  
        end
      end
    end
 end
  
 #metodi di ricerca delle band---------------------------------------------------------------------------------------------------------- 
  def self.search(params)
    if params
       where('name ILIKE ?', "%#{params[:search]}%") 
    else
      scoped
    end
  end   
  
  #metodo per aggiungere i generi
  def music_genre(params)
    if not params[:genre].nil?
      params[:genre].each do |k,v|
          self.band_genre_relationships.create(genre_id:Genre.find(k).id)
      end  
    end
  end
end
