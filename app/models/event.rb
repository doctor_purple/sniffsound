class Event < ActiveRecord::Base
  require 'date'
  include ActiveModel::EventValidators
  
  
  attr_accessible :datetime, :description, :name, :lat, :lon, :poster
  
  belongs_to :user
  has_many :user_event_relationships, dependent: :destroy
  has_many :participating_users, through: :user_event_relationships, source: :user
  has_many :event_band_relationships, dependent: :destroy
  has_many :bands, through: :event_band_relationships, source: :band
  has_many :comments, dependent: :destroy
  
  has_attached_file :poster, styles:{medium:"300x300#",thumb:"100x100#"}, :default_url => "/system/events/posters/missing/thumb.png"
  
  #VALID_DATE_REGEX = /(?<day>\d{1,2})\/(?<month>\d{1,2})\/(?<year>\d{4})/
  validates :name, presence:true, length: {minimum:1,maximum:30}
  validates :datetime, presence:true, date_time:true #almeno n ore da adesso
  validates :description, presence:true, length: {minimum:10,maximum:140}
  validates :lat, presence:true
  validates :lon, presence:true
  
  #---------------------Comments-----------------------------------------------------------------------------------------------------------------------

  def comment(content,user_id)
    result = self.comments.create(content: content, user_id: user_id)
    return result.valid?
  end
  
  #---------------------Bands--------------------------------------------------------------------------------------------------------------------------
  
  def build_bands(params)
        params[:bands].each do |band, val|
          Rails.logger.debug("Id e valore: #{band}, #{val}")
          if Integer(val) == 1
            self.event_band_relationships.build(band_id:Integer(band))
          end
        end        
  end
  
  def update_bands(params)
        params[:bands].each do |band, val|
        if Integer(val) == 1
          self.event_band_relationships.find_or_create_by_band_id(band_id:band)
        elsif Integer(val) == 0 && !(relationship=self.event_band_relationships.find_by_band_id(band)).nil?
          relationship.delete
        end
      end
  end
  
  def save_unique
    #---------------------------------------Qui inserire condizione con query per controllare range spaziale/temporale----------------------------------
    begin
      self.save
    rescue Exception::ActiveRecord::RecordNotUnique => e
      if e.is_a? ActiveRecord::RecordNotUnique
        self.errors[:base] = "Solo un evento ammesso in quel luogo in quel momento"
        false
      else
        true
      end
    end
  end
  #-----------------------------------------Liste band per selezione band---------------------------------------------------------------------------------
  def band_selection_lists_participating
   @bands_participating = self.bands.all.to_json.html_safe
  end
  def band_selection_lists_not_participating
    @bands = Band.where("id NOT IN (SELECT band_id FROM event_band_relationships WHERE event_id=?)", self.id).to_json.html_safe
  end
  
  
  
  #----------------------------------------------------RICERCA EVENTI------------------------------------------------------------------
  def self.search(params)
    if params 
      name = params[:name]
      lat = params[:vicino_lat].to_f
      lon = params[:vicino_lon].to_f
      distance = params[:distance].to_f
      cordinates = false
      
      if lat!=0 && lon!=0 #le coordinate con il submit sono 0,0
        cordinates = true    
      end
      
      
      
      #considero tutti i casi
      if !params[:startDateHidden].blank? && !params[:endDateHidden].blank? 
          startDateTime = splitting(params[:startDateHidden])
          endDateTime = splitting(params[:endDateHidden])
           
          if cordinates
              search_name(name).search_datetime(startDateTime, endDateTime).search_area(lat, lon, distance).order('datetime ASC')
          else
               search_name(name).search_datetime(startDateTime, endDateTime).order('datetime ASC')
          end  
           
      elsif !params[:startDateHidden].blank? && params[:endDateHidden].blank?
          startDateTime = splitting(params[:startDateHidden])
          if cordinates
              search_name(name).search_datetime_after(startDateTime).search_area(lat, lon, distance).order('datetime ASC')

          else
              search_name(name).search_datetime_after(startDateTime).order('datetime ASC')
          end
          
      elsif !params[:endDateHidden].blank? && params[:startDateHidden].blank?
          endDateTime = splitting(params[:endDateHidden])
          if cordinates
              search_name(name).search_datetime_before(endDateTime).search_area(lat, lon, distance).order('datetime ASC')
    
          else
              search_name(name).search_datetime_before(endDateTime).order('datetime ASC') 
          end  
          
          
      elsif params[:startDateHidden].blank? && params[:endDateHidden].blank? #se le date sono tutte null, cerca solo il nome
           if cordinates
              search_name(name).search_area(lat, lon, distance).order('datetime ASC')
          else
              search_name(name).order('datetime ASC')
          end  
      
      else
          return scoped    
      end   
    end                     
  end
  

  #--------------------------------------------------------------RICERCA EVENTI NELLA DASHBOARD-------------------------------------------------------
  def self.search_dashboard(isPast) #la ricerca nella dashboard deve tenere conto degli ultimi 30 giorni o dei futuri 30 giorni
    now = DateTime.now
    past30days = now - 30.days
    next30days = now + 30.days
    
   
    if isPast
        search_datetime(past30days, now).order('datetime ASC')
    else 
        search_datetime(now, next30days).order('datetime DESC')
    end  
  end
  
  
  
  #--------------------------METODI GENERALI PER LA RICERCA DEGLI EVENTI, DA COMBINARE INSIEME PER METODI PIÙ COMPLESSI----------------------------------
  def self.search_all_future
      now = DateTime.now
      search_datetime_after(now).order('datetime ASC')
    
  end
  
  def self.search_all_past
      now = DateTime.now
      search_datetime_before(now).order('datetime DESC')
  end  
  
  def self.search_events_live #eventi live in questo momento
    inizioEvento = DateTime.now - 3.hours
    fineEvento = DateTime.now + 1.hours
    

    search_datetime(inizioEvento, fineEvento).order('datetime DESC')  
  end
  
  
  def self.search_events_participated(user)
    user.participating_events.search_all_past
  end
  
  def self.search_events_participating(user)
    user.participating_events.search_all_future
  end
  
  
  def self.search_most_popular
    #questa query è stata testata su PG e funziona in sql puro. Meglio ottimizzare con rails 
    # find_by_sql("SELECT *
    # FROM public.events
    # WHERE id IN (SELECT event_id
                 # FROM public.user_event_relationships
                 # GROUP BY event_id
                 # ORDER BY COUNT(user_id) DESC
                 # LIMIT 10)")
       
  end
  
  
  #---------------------------------------------------METODI COMUNI A TUTTE LE RICERCHE--------------------------------------------------------------------------------------
  
  #query tra due date
  def self.search_datetime(startDateTime, endDateTime)
    where('datetime BETWEEN ? AND ?', startDateTime.strftime('%Y-%m-%d %H:%M:%S'), endDateTime.strftime('%Y-%m-%d %H:%M:%S'))
  end
  
  #query per date maggiori di un certo valore
  def self.search_datetime_after(startDateTime)
    where('datetime > ?', startDateTime.strftime('%Y-%m-%d %H:%M:%S'))
  end
  
  #query per date minori di un certo valore
  def self.search_datetime_before(endDateTime)
    where('datetime < ?', endDateTime.strftime('%Y-%m-%d %H:%M:%S'))
  end
  
  #query per la ricerca del nome
  def self.search_name(name)    
       where('events.name ILIKE ?',  "%#{name}%")
  end
  
  #query per la ricerca delle coordinate in un area
  def self.search_area(lat, lon, distance)
    where('lat BETWEEN ? AND ? AND lon BETWEEN ? AND ?', lat - distance, lat + distance, lon - distance, lon + distance)
  end
  
  #join per la ricerca in base al genere delle band che vi partecipano
  def self.search_genre(genre_id)
    joins(:bands => :music_genres).where('genre_id = ?', genre_id).uniq
  end
  



  #------------------------------------------------------METODI SPLITTING DELLA DATA-------------------------------------------------------------------
  def self.splitting(date) #splitting della data
      
       day = date.split('/')[0].to_i
       month = date.split('/')[1].to_i
       year = date.split('/')[2].to_i
       
       DateTime.civil(year, month, day, 0, 0) 
  end
  
  def self.splitting_time(date, hour, minute) #splitting della data, e dell'ora (ore, minuti)
      
      day = date.split('/')[0].to_i
      month = date.split('/')[1].to_i
      year = date.split('/')[2].to_i
      hour = hour.to_i
      minute = minute.to_i
      
      DateTime.civil(year, month, day, hour, minute)  
  end

end
  
     

