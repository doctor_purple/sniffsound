class BandGenreRelationship < ActiveRecord::Base
  attr_accessible :band_id, :genre_id
  
  belongs_to :band
  belongs_to :genre, class_name: "Genre"
end
