class User < ActiveRecord::Base
  include ActiveModel::UserValidators
  attr_accessible :email, :fullname, :password, :password_confirmation, :twitter_uid, 
                  :facebook_uid, :birthdate, :city, :profilepic, :avatar_remote_url, :remember_me, :updating_password
  
  @check_password = true

  has_attached_file :profilepic, styles:{medium:"300x300#",thumb:"100x100#"}, :default_url=>"/system/events/posters/missing/thumb.png"
  
  has_many :events, dependent: :destroy
  has_many :user_event_relationships, dependent: :destroy
  has_many :participating_events, through: :user_event_relationships, source: :event
  has_many :user_genre_relationships, dependent: :destroy
  has_many :genres, through: :user_genre_relationships, source: :genre
  has_many :favourite_genres, through: :user_genre_relationships, source: :genre
  
  has_many :user_band_relationships, dependent: :destroy
  has_many :bands, through: :user_band_relationships, source: :band
  
  has_many :user_user_relationships, foreign_key: "follower_id", dependent: :destroy
  has_many :followed_users, through: :user_user_relationships, source: :followed
  has_many :user_reverse_relationships, foreign_key: :followed_id, 
                                   class_name: "UserUserRelationship",
                                   dependent: :destroy
  has_many :followers, through: :user_reverse_relationships, source: :follower
  
  
  has_many :fan_band_relationships, dependent: :destroy
  has_many :favourite_bands, through: :fan_band_relationships, source: :band
  
  has_many :comments, dependent: :destroy
  
  has_secure_password
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  
  before_save { |user| user.email = email.downcase } #forza la scrittura della mail prima del rollback, e controlla che non ci sia già
  before_save :create_remember_token
  
  #metodi per validare i campi dell'utente
  validates :email, presence: true,
                    format: {with: VALID_EMAIL_REGEX},
                    uniqueness: { case_sensitive: false,
                                   message: "email in uso" }
  validates :fullname, presence: true,
                      length: {minimum: 5, maximum: 30}
  validates :password, length: { minimum: 6 },
                      presence: { on: :create }, if: :should_validate_password?
  validates :password_confirmation, presence: true, if: :should_validate_password?
  validates :birthdate, presence:true, birth_date:true

#---------------------------------------metodi per i fan delle band---------------------------------------------------------------------
def fan!(band)
  self.fan_band_relationships.create(band_id:band.id)
end

def unfan!(id)
  self.fan_band_relationships.find_by_id(Integer(id)).destroy
end 

def fan?(band)
  !self.fan_band_relationships.find_by_band_id(band.id).nil?
end
 
#---------------------------------------metodi per le relazioni di partecipazione-----------------------------------------------------------  
  def participating?(event)
    user_event_relationships.find_by_event_id(event.id)
  end
  
  def participate!(event)
    user_event_relationships.create!(event_id: event.id)
  end
  
  def unparticipate!(event)
    user_event_relationships.find_by_event_id(event.id).destroy
  end
  
#--------------------------------------------metodi per le relazioni tra gli utenti-----------------------------------------------------------

  def following?(other_user)
    user_user_relationships.find_by_followed_id(other_user.id)
  end

  def follow!(other_user)
    user_user_relationships.create!(followed_id: other_user.id)
  end

  def unfollow!(id)
    user_user_relationships.find_by_id(Integer(id)).destroy
  end


  
#-----------------------------------metodi per la ricerca degli utenti----------------------------------------------------------------------    
  def self.search(params)
    if params
      where('fullname ILIKE ?',  "%#{params[:search]}%") #ILIKE per il case sensitive = false
     else
       scoped
     end  
   end   
   
#-----------------------------------metodi per la relazione di generi preferiti
def saveFavouriteGenres(params)
  if not params.nil?
    params.each do |k,i|
      self.user_genre_relationships.create(genre_id: Genre.find(k).id)
    end
  end
end

#-----------------------------------Per aggiornare il profilo
def update_attributes_and_genres(params)
  @check_password = false
  if not self.user_genre_relationships.nil? 
    self.user_genre_relationships.each do |ob|
      ob.destroy
    end
  end
  self.saveFavouriteGenres(params[:genre])
  self.update_attributes(params[:user])
end

def should_validate_password? #per evitare che richieda la presenza della password quando aggiorno il profilo
  @check_password && true
end

#-----------------------------------metodo per creare il rember_token usato per la sessione utente----------------------------------
 
def create_remember_token
  self.remember_token = SecureRandom.base64 #metodo per la cifratura del token
end 
    
end
