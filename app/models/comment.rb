class Comment < ActiveRecord::Base
  attr_accessible :content, :event_id, :user_id
  validates_presence_of [:user_id, :event_id, :content]
  validates :content, length: {minimum:1, maximum:140}
  
  belongs_to :event
  belongs_to :user
end
