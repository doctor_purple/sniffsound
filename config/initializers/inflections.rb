# Be sure to restart your server when you modify this file.

# Add new inflection rules using the following format
# (all these examples are active by default):
# ActiveSupport::Inflector.inflections do |inflect|
#   inflect.plural /^(ox)$/i, '\1en'
#   inflect.singular /^(ox)en/i, '\1'
#   inflect.irregular 'person', 'people'
#   inflect.uncountable %w( fish sheep )
# end
#
# These inflection rules are supported but not enabled by default:
# ActiveSupport::Inflector.inflections do |inflect|
#   inflect.acronym 'RESTful'
# end


#qui vengono scritte le regole per il pluralize che ci servono
ActiveSupport::Inflector.inflections do |inflect|
  inflect.irregular 'errore', 'errori'
  inflect.irregular 'risultato', 'risultati'
  inflect.irregular 'Evento', 'Eventi'
  inflect.irregular 'creato', 'creati'
  inflect.irregular 'Utente', 'Utenti'
end
