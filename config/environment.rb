# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
SniffSound::Application.initialize!

#ActionMailer
SniffSound::Application.configure do
  config.action_mailer.delivery_method = :smtp
end