module UserValidators
  class BirthDateValidator < ActiveModel::EachValidator #validatore della data di nascita
     def validate_each(object, attribute, value)
       date_now = Date.today
       tolerance = 14
       
        if value.year > (date_now.year - tolerance)
         object.errors[attribute] = "Ehi non fare il furbetto: devi essere nato almeno #{tolerance} anni fa"
       end
     end
  end
end