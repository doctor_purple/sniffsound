module EventValidators #modulo con tutti i validatori personalizzati

    class DateTimeValidator < ActiveModel::EachValidator #validatore della data dell'evento
       def validate_each(object, attribute, value)
         now = Time.now
         tolerance = 6
         
         unless value > (now + tolerance.hours + now.gmtoff.seconds )
           object.errors[attribute] = "L'evento deve iniziare fra almeno #{tolerance} ore"
         end    
       end
     end
end